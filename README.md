# Sofort Lib

This is a refactored version of Sofort's original API PHP lib 2.01 but namespaced instead of using require_once everywhere.

Use composer:

'''

"require": {
    "ecopharma/sofortlib": "*"
},
"repositories": [
    {
        "type": "vcs",
        "url":  "git@bitbucket.org:ecopharmabvba/sofortlib.git"
    }
],

'''

