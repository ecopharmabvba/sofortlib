<?php

namespace Ecopharma\Sofortlib\Lib\Elements;

/**
 * @author SOFORT AG (integration@sofort.com)
 *
 * @link http://www.sofort.com/
 *
 * Copyright (c) 2013 SOFORT AG
 *
 * Released under the GNU LESSER GENERAL PUBLIC LICENSE (Version 3)
 * [http://www.gnu.org/licenses/lgpl.html]
 */

/**
 * Implementation of simple text.
 */
class SofortText extends SofortElement
{
    public $text;

    public $escape = false;

    /**
     * Constructor for SofortText.
     *
     * @param string $text
     * @param bool   $escape (default false)
     * @param bool   $trim   (default true)
     *
     * @return \Ecopharma\Sofortlib\Lib\Elements\SofortText
     */
    public function __construct($text, $escape = false, $trim = true)
    {
        $this->text = $trim ? trim($text) : $text;
        $this->escape = $escape;
    }

    /**
     * Renders the element (override).
     *
     * @see SofortElement::render()
     *
     * @return string
     */
    public function render()
    {
        return $this->escape ? htmlspecialchars($this->text) : $this->text;
    }
}
